package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {
	@FXML
	public TextField portTextField;

	@FXML
	public Button startTransmissionButton;

	@FXML
	public Button stopTransmisiionButton;

	UDPServerThread udpServerThread;
	TCPServerThread tcpServerThread;

	@FXML
	public void initialize() {
		startTransmissionButton.setDisable(false);
		stopTransmisiionButton.setDisable(true);

	}

	@FXML
	private void handleStartButtonAction(ActionEvent event) throws IOException {
		if (validateListeningPort()) {
			udpServerThread = new UDPServerThread(Integer.parseInt(portTextField.getText()));
			tcpServerThread = new TCPServerThread(Integer.parseInt(portTextField.getText()));
			udpServerThread.start();
			tcpServerThread.start();
			startTransmissionButton.setDisable(true);
			stopTransmisiionButton.setDisable(false);
			portTextField.setEditable(false);

		} else {
			Alert alert = new Alert(AlertType.ERROR, "Wrong port number");
			alert.showAndWait();
		}
	}

	@SuppressWarnings("deprecation")
	@FXML
	private void handleStopTransmisiion(ActionEvent event) throws IOException, InterruptedException {

		startTransmissionButton.setDisable(false);
		stopTransmisiionButton.setDisable(true);
		portTextField.setEditable(true);

		udpServerThread.stopMe();
		Thread.sleep(2);
		tcpServerThread.stopMe();

		Thread.sleep(2);

		udpServerThread.stop();
		Thread.sleep(2);
		tcpServerThread.stop();
	}

	private boolean validateListeningPort() {
		if (portTextField.getText().length() != 0)
			try {
				return Integer.parseInt(portTextField.getText()) > 0
						&& Integer.parseInt(portTextField.getText()) < 65535;
			} catch (NumberFormatException e) {
				Alert alert = new Alert(AlertType.ERROR, "Wrong port number");
				alert.showAndWait();
			}
		return false;
	}

}
