package application;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;

public class TCPServerThread extends Thread {

	private int port;
	private boolean finished = false;
	ServerSocket welcomeSocket;
	Socket connectionSocket;
	int readedBytes = 0;
	LocalDateTime currentTime;
	private int bufferSize;
	private PrintWriter out;
	private double timeBeetweenEachData;
	private double transferSpeed;
	private String dataReceived;
	private String time;
	private String speed;
	private BufferedReader inFromClient;
	private DataInputStream in;
	private int singleRead = 0;

	public ServerSocket getSocket() {
		return welcomeSocket;
	}

	public TCPServerThread(int port) {
		this.port = port;
		initialize();
	}

	private void initialize() {
		try {
			welcomeSocket = new ServerSocket(port);
		} catch (IOException e) {
			System.out.println("Error on socket creation!");
		}
	}

	public void stopMe() throws InterruptedException {
		finished = true;
		try {
			out.close();
			if (!connectionSocket.isClosed()) {
				connectionSocket.close();
			}
			if (!welcomeSocket.isClosed()) {
				welcomeSocket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (!finished) {
			try {
				out = new PrintWriter(new FileWriter("outputTCP.txt", true), true);
				connectionSocket = welcomeSocket.accept();
			} catch (IOException e) {
				try {
					welcomeSocket.close();
				} catch (IOException e1) {
					System.out.println("Closing socket problem");
				}
				this.stop();
			}
			String clientSentence = null;
			try {
				inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
				clientSentence = inFromClient.readLine().trim();
				if (clientSentence.substring(0, 4).equals("SIZE")) {
					bufferSize = Integer.parseInt(clientSentence.substring(5).trim());
				}
				in = new DataInputStream(connectionSocket.getInputStream());
			} catch (IOException e) {
				try {
					connectionSocket.close();
					welcomeSocket.close();
				} catch (IOException e1) {
					System.out.println("Closing socket problem");
				}
				this.stop();
			}

			byte[] buffer = new byte[bufferSize];
			long startTime = System.currentTimeMillis();
			while (singleRead != -1) {
				long singleStartTime = System.currentTimeMillis();
				singleRead = 0;
				try {
					singleRead = in.read(buffer);
				} catch (IOException e) {
					this.stop();
				}
				timeBeetweenEachData = ((double) System.currentTimeMillis() - (double) singleStartTime) / 1000.0;
				readedBytes += singleRead;
				transferSpeed = (double) singleRead / timeBeetweenEachData;
				dataReceived = (readedBytes < 1024 ? readedBytes + "bytes" : readedBytes / 1024 + "kbytes");
				time = String.valueOf((double)((System.currentTimeMillis() - startTime) / 1000.0d));
				speed = (transferSpeed < 1024.0 ? (transferSpeed + " bytes/sec")
						: (transferSpeed / 1024.0 + " kbytes/sec"));

				out.println("W�tek TCP: odebrano: " + dataReceived + " danych w czasie: " + time + "sek z pr�dko�ci�: "
						+ speed);

				System.out.println("W�tek TCP: odebrano: " + dataReceived + " danych w czasie: " + time
						+ "sek z pr�dko�ci�: " + speed);

			}
			try {
				out.close();
				connectionSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			readedBytes = 0;
			singleRead = 0;
		}
	}

}
