package application;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UDPServerThread extends Thread {

	private int port;
	private boolean finished;
	private DatagramSocket socket;
	byte[] firstReceiveData = new byte[1024];
	private int bufferSize;
	private int readedBytes = 0;
	PrintWriter out;
	private double transferSpeed;
	private String dataReceived;
	private String time;
	private String speed;

	public UDPServerThread(int port) {
		this.port = port;
		initialize();
	}

	@SuppressWarnings("deprecation")
	public void stopMe() throws InterruptedException {
		finished = true;
		out.close();
		socket.close();
	}

	private void initialize() {

		try {
			socket = new DatagramSocket(port);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (!finished) {
			DatagramPacket receivePacket = new DatagramPacket(firstReceiveData, firstReceiveData.length);
			try {
				out = new PrintWriter(new FileWriter("outputUDP.txt", true), true);
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			try {
				socket.receive(receivePacket);
			} catch (IOException e1) {
				this.stop();
			}
			String clientSentence = new String(receivePacket.getData()).trim();
			System.out.println(clientSentence);
			try {
			if (clientSentence.substring(0, 4).equals("SIZE")) {
				bufferSize = Integer.parseInt(clientSentence.substring(5));
			}}
			catch (StringIndexOutOfBoundsException e) {
				continue;
			}
			byte[] buffer = new byte[bufferSize];
			long startTime = System.currentTimeMillis();
			do {
				receivePacket = new DatagramPacket(buffer, buffer.length);
				long singleStartTime = System.currentTimeMillis();
				try {
					socket.receive(receivePacket);
				} catch (IOException e) {
					
				}
				int singleRead = receivePacket.getLength();
				double timeBeetweenEachData = ((double) System.currentTimeMillis() - (double) singleStartTime) / 1000.0;
				readedBytes += singleRead;
				transferSpeed = (double) singleRead / timeBeetweenEachData;
				dataReceived = (readedBytes < 1024 ? readedBytes + "bytes" : readedBytes / 1024 + "kbytes");
				time = String.valueOf((System.currentTimeMillis() - startTime) / 1000);
				speed = (transferSpeed < 1024 ? (transferSpeed + " bytes/sec")
						: (transferSpeed / 1024 + " kbytes/sec"));

				out.println("W�tek UDP: odebrano: " + dataReceived + " danych w czasie: " + time + "sek z pr�dko�ci�: "
						+ speed);

				System.out.println("W�tek UDP: odebrano: " + dataReceived + " danych w czasie: " + time
						+ "sek z pr�dko�ci�: " + speed);

			} while (!(new String(receivePacket.getData()).trim().equals("FINE")));
			readedBytes = 0;
		}

	}

	public DatagramSocket getSocket() {
		return socket;
	}
}
